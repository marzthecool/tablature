defmodule Tablature do
  def parse(tab) do
    tab
    |> String.split()
    |> Enum.map(fn line -> parse_line(line) end)
    |> Enum.filter(fn lista -> lista != [] end)
    |> Enum.zip
    |> Enum.map(fn t -> Tuple.to_list(t) |> Enum.join(" ") end)
    |> Enum.join(" ")
  end

  def parse_line(line) do
    Regex.scan(~r/\d/, line)
    |> List.flatten
    |> Enum.map(fn e -> String.at(line,0) <> e end)
  end


  # def parse_line(tab) do
  #   Regex.scan(~r/\d+/, tab)
  #   |> List.flatten
  #   |> Enum.map(fn n -> "B" <> n end)
  #   |> Enum.join(" ")
  # end
end
